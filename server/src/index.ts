import EventSource from "eventsource";
import fetch from "node-fetch";
import yargs from "yargs";
import version from "./version";
import { ServiceManifest } from "pbedat-dashboard-core";

const { argv } = yargs.option("streamName", { required: true });
const { streamName } = argv;

const streamBin = "https://streambin.pbedat.de";

var events = new EventSource(`${streamBin}/streams/${streamName}`);

events.addEventListener("open", () => {
  process.stdout.write("[streambin] connected\n");
});

events.addEventListener("error", (ev: any) => {
  process.stdout.write(`[streambin] error ${ev.message}\n`);
});

let messageBuffer: any[] = [];

events.addEventListener("connect", async ev => {
  console.log("conncted", ev);
  const response = await fetch(`${streamBin}/streams/${streamName}/in`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      type: "capabilities",
      data: JSON.stringify({
        capabilities: {
          history: `https://${streamName}.proxy.http.pbedat.de/messages`
        }
      })
    })
  });

  if (!response.ok) {
    console.log("[streambin] couldnt send init", response.status);
  }
});

events.addEventListener("input", (ev: any) => {
  console.log(`[streambin] input event`, ev);
  const input = JSON.parse(ev.data);

  switch (input.type) {
    case "message":
      messageBuffer = [
        ...(messageBuffer.length > 200
          ? messageBuffer.slice(1)
          : messageBuffer),
        JSON.parse(input.data)
      ];
      break;
  }
});

events.onmessage = ev => console.log("message", ev.type, ev.data);

var requests = new EventSource(
  `http://http.pbedat.de/api/${streamName}.proxy.http.pbedat.de/requests`
);

const uptime = new Date().toString();

requests.onopen = () => process.stdout.write("[http-over-sse] connected\n");
requests.onerror = (ev: any) =>
  process.stdout.write(`[https-over-sse] error ${ev.message}\n`);
requests.onmessage = async msg => {
  const proxyRequest = JSON.parse(msg.data);

  const { uid, request } = proxyRequest;
  const { method, path } = request;

  if (method === "GET" && path === "/manifest") {
    const manifest: ServiceManifest = {
      serviceId: "stream-chat-server",
      version,
      uptime
    };

    const response = await fetch(
      `http://http.pbedat.de/api/${streamName}.proxy.http.pbedat.de/requests/${uid}/responses`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          status: 200,
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(manifest)
        })
      }
    );

    if (!response.ok) {
      process.stdout.write(
        `[http-over-sse] response failed ${response.status}`
      );
    }
  }

  if (method !== "GET" || path !== "/messages") {
    return;
  }
  const response = await fetch(
    `http://http.pbedat.de/api/${streamName}.proxy.http.pbedat.de/requests/${uid}/responses`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        status: 200,
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(messageBuffer)
      })
    }
  );

  if (!response.ok) {
    process.stdout.write(`[http-over-sse] response failed ${response.status}`);
  }

  console.log("request", proxyRequest);
};
