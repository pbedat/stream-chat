# Roadmap

[ ] Handle out stream disconnects by publishing the new streamToken
[x] add a ping to streambin control stream

[ ] better use in/out stream per client? useful for peer to peer

client 1 client 2 -+
(in) (in) <--+
(out) <-----> (out)

Protocol:

1: create stream
1: send link: ?streamName
2: connect streams/{streamName}?ownStreamName
1: connect streams/{ownStreamName}
