export interface AppState {
  config: ChatConfig | null;
  messageBuffer: ChatMessage[];
}

export interface ChatMessageEvent {
  id: string;
  timestamp: string;
  nickName: string;
  text: string;
}

export interface ChatMessage {
  id: string;
  timestamp: Date;
  text: string;
  nickName: string;
  isOwn: boolean;
}

export interface ChatConfig {
  nickName: string;
  streamName: string;
}
