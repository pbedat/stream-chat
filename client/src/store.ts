import { createStore, applyMiddleware } from "redux";

import rootReducer from "./state/reducer";
import createSagaMiddleware from "@redux-saga/core";

export const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

export default store;
