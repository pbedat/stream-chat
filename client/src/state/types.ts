import { ChatMessage } from "../types";

export interface AppState {
  messageBuffer: ChatMessage[];
  nickName: string;
  connected: boolean;
}
