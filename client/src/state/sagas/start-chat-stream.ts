import { eventChannel, EventChannel, END } from "redux-saga";
import { select, put, call, take } from "redux-saga/effects";
import { createAction, ActionType, getType } from "typesafe-actions";

import { STREAM_BIN_BASE_URL } from "../constants";
import { ChatMessage, ChatMessageEvent } from "../../types";
import selectors from "../selectors";
import actions from "../actions";

export default startChatStream;

function* startChatStream(streamName: string) {
  const events = yield call(connectToStream, streamName);
  yield call(subscribeToChatEvents, events);
}

function connectToStream(streamName: string) {
  return eventChannel(emit => {
    const events = new EventSource(
      `${STREAM_BIN_BASE_URL}/streams/${streamName}/out`
    );

    events.onopen = () => emit(chatEvents.connected);
    events.onmessage = ev => emit({ type: ev.type, payload: ev.data });
    events.onerror = err => {
      console.error("[streambin::error] %s", err);
      emit(chatEvents.disconnected);
    };

    events.addEventListener(
      "capabilities",
      async (ev: any) => {
        const msg = ev as MessageEvent;
        const { capabilities } = JSON.parse(msg.data);
        if (capabilities.history) {
          const response = await fetch(capabilities.history);
          if (!response.ok) {
            console.error(
              `unable to retrieve history ${response.status} ${
                response.statusText
              }`
            );
            return;
          }

          const messages = await response.json();

          emit(
            chatEvents.history(
              messages.map(({ timestamp, ...rest }: any) => ({
                timestamp: new Date(timestamp),
                ...rest
              }))
            )
          );
        }
      },
      { once: true }
    );

    return () => events.close();
  });
}

const chatEvents = {
  connected: createAction("connected"),
  disconnected: createAction("disconnected"),
  message: createAction("message", _ => (message: ChatMessage) => _(message)),
  history: createAction("history", _ => (messages: ChatMessage[]) =>
    _(messages)
  )
};

function* subscribeToChatEvents(
  events: EventChannel<ActionType<typeof chatEvents>>
) {
  const nickName = yield select(selectors.getNickName);
  for (;;) {
    const event = yield take(events);
    switch (event.type) {
      case END:
        return;
      case getType(chatEvents.message):
        const msg = JSON.parse(event.payload) as ChatMessageEvent;
        yield put(
          actions.receiveMessage({
            ...msg,
            isOwn: nickName === msg.nickName,
            timestamp: new Date(msg.timestamp)
          })
        );
        break;
      case getType(chatEvents.history):
        yield put(
          actions.loadHistory(
            event.payload.map((msg: ChatMessageEvent) => ({
              ...msg,
              isOwn: nickName === msg.nickName
            }))
          )
        );
        break;
    }
  }
}
