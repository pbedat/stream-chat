import {
  fork,
  call,
  takeEvery,
  select,
  take,
  cancel,
  delay
} from "redux-saga/effects";
import { Task } from "redux-saga";
import { getType, ActionType } from "typesafe-actions";
import uuid from "uuid";

import actions from "../actions";
import selectors from "../selectors";
import { AppState } from "../../types";
import { STREAM_BIN_BASE_URL } from "../constants";

export default handleUiEffects;

function* handleUiEffects(streamName: string) {
  yield takeEvery(getType(actions.submitMessage), sendMessagesTo(streamName));
  yield fork(persist);
}

function sendMessagesTo(streamName: string) {
  return function* sendMessage(
    action: ActionType<typeof actions.submitMessage>
  ) {
    const nickName = yield select(selectors.getNickName);
    const body = JSON.stringify({
      type: "message",
      data: JSON.stringify({
        id: uuid(),
        nickName,
        timestamp: new Date(),
        text: action.payload
      })
    });
    yield call(fetch, `${STREAM_BIN_BASE_URL}/streams/${streamName}/in`, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body
    });
  };
}

/**
 * Persists received messages in the localStorage.
 * Debounced (5000ms)
 */
function* persist() {
  let task: Task | null = null;
  for (;;) {
    yield take(getType(actions.receiveMessage));
    if (task) {
      yield cancel(task);
    }
    task = yield fork(saveBuffer);
  }
}

function* saveBuffer() {
  yield delay(5000);
  const storageKey = yield select(
    (state: AppState) => `${state.config!.streamName}/${state.config!.nickName}`
  );
  const buffer = yield select(selectors.getBuffer);
  window.localStorage.setItem(storageKey, JSON.stringify(buffer));
}
