import Uri from "urijs";
import { fork, call, put } from "redux-saga/effects";

import actions from "../actions";
import { ChatMessage } from "../../types";
import handleUiEffects from "./handle-ui-effects";
import startChatStream from "./start-chat-stream";

function* bootstrapApp() {
  const { streamName } = yield call(setupState);

  yield fork(handleUiEffects, streamName);
  yield fork(startChatStream, streamName);
}

function* setupState() {
  const { streamName, nickName } = new Uri(window.location.href).search(true);

  // initialize or restore the buffer
  const persistedBuffer = window.localStorage.getItem(
    `${streamName}/${nickName}`
  );
  const buffer: ChatMessage[] = persistedBuffer
    ? JSON.parse(persistedBuffer, (key, val) =>
        key === "timestamp" ? new Date(val) : val
      )
    : [];

  yield put(
    actions.setup({
      streamName,
      nickName,
      buffer
    })
  );

  return { streamName };
}

export default bootstrapApp;
