import { AppState } from "../types";

const selectors = {
  getNickName(state: AppState) {
    return state.config ? state.config.nickName : undefined;
  },
  getStreamName(state: AppState) {
    return state.config ? state.config.streamName : undefined;
  },
  getBuffer(state: AppState) {
    return state.messageBuffer;
  }
};

export default selectors;
