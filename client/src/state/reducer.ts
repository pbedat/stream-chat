import { uniqBy } from "lodash";
import { ChatMessage, AppState, ChatConfig } from "../types";
import { Reducer, combineReducers } from "redux";

import actions from "./actions";
import { ActionType, getType } from "typesafe-actions";

type Actions = ActionType<typeof actions>;

const messageBuffer: Reducer<ChatMessage[], Actions> = (
  state = [],
  action: ActionType<typeof actions>
) => {
  switch (action.type) {
    case getType(actions.setup):
      return action.payload.buffer;
    case getType(actions.receiveMessage):
      return [...state, action.payload];
    case getType(actions.loadHistory):
      return uniqBy(state.concat(action.payload), msg => msg.id);
  }

  return state;
};

const config: Reducer<ChatConfig | null, Actions> = (
  state = null,
  action: Actions
) => {
  switch (action.type) {
    case getType(actions.setup):
      const { nickName, streamName } = action.payload;
      return { nickName, streamName };
  }
  return state;
};

export default combineReducers<AppState>({
  messageBuffer,
  config
});
