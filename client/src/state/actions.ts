import { createAction } from "typesafe-actions";
import { ChatMessage } from "../types";

const actions = {
  submitMessage: createAction("SUBMIT_MESSAGE", _ => (text: string) => _(text)),
  receiveMessage: createAction("RECEIVE_MESSAGE", _ => (message: ChatMessage) =>
    _(message)
  )
};

interface SetupArgs {
  nickName: string;
  streamName: string;
  buffer: ChatMessage[];
}

const effects = {
  toggleConnection: createAction(
    "TOGGLE_CONNECTION",
    _ => (connected: boolean) => _(connected)
  ),
  setup: createAction("SETUP", _ => (config: SetupArgs) => _(config)),
  loadHistory: createAction("LOAD_HISTORY", _ => (messages: ChatMessage[]) =>
    _(messages)
  )
};

export default {
  ...actions,
  ...effects
};
