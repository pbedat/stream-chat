import React from "react";
import ReactDOM from "react-dom";

import "./index.css";
import AppContainer from "./view/containers/app-container";
import bootstrapApp from "./state/sagas";

import store, { sagaMiddleware } from "./store";
import { Provider } from "react-redux";

sagaMiddleware.run(bootstrapApp);

ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById("root")
);
