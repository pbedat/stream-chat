export type Either<T> = [undefined | unknown, T | undefined] | [unknown];
export function isError<T>(either: Either<T>): either is [unknown, any] {
  return either[0] !== undefined;
}
