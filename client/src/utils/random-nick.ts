import { Either } from "./either";

async function createRandomNick(): Promise<Either<string>> {
  const response = await fetch("https://api.codetunnel.net/random-nick", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: "{}"
  });

  const result: RandomNickResponse = await response.json();

  if (!response.ok || !result.success) {
    return [result.error.message];
  }

  return [undefined, result.nickname.replace(/\s+/g, "-")];
}

interface RandomNickResponse {
  success: boolean;
  nickname: string;
  error: {
    message: string;
    code: string;
  };
}

export default createRandomNick;
