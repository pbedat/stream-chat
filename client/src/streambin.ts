export default function createClient(
  streamName: string,
  url: string = "https://streambin.pbedat.de"
) {
  async function push(payload: any, type: string = "message") {
    const { ok } = await fetch(`${url}/streams/${streamName}/in`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ payload, type })
    });

    return ok;
  }

  function subscribeTo() {
    return new Promise<EventSource>((resolve, reject) => {
      const events = new EventSource(`${url}/streams/${streamName}/out`);

      events.addEventListener("open", () => resolve(events), { once: true });
      events.addEventListener("error", () => reject(), { once: true });
    });
  }

  function createStream() {
    return new Promise<{
      events: EventSource;
      push: typeof push;
      subscribeTo: typeof subscribeTo;
    }>((resolve, reject) => {
      const events = new EventSource(`${url}/streams/${streamName}`);

      events.addEventListener(
        "open",
        () =>
          resolve({
            events,
            push,
            subscribeTo
          }),
        { once: true }
      );
      events.addEventListener("error", () => reject(), { once: true });
    });
  }

  return {
    createStream
  };
}
