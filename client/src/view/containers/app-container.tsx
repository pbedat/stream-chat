import { AppState } from "../../types";
import { Dispatch } from "redux";
import actions from "../../state/actions";
import { connect } from "react-redux";
import App from "../components/app";

const mapStateToProps = (state: AppState) => {
  return {
    buffer: state.messageBuffer
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onSubmit(msg: string) {
      dispatch(actions.submitMessage(msg));
    }
  };
};

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default AppContainer;
