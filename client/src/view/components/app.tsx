import React from "react";

import { ChatMessage } from "../../types";
import { useNotifier } from "./notifier";
import Layout from "./layout";
import Chat from "./chat";

interface Props {
  buffer: ChatMessage[];
  onSubmit(msg: string): void;
}

const App: React.FC<Props> = props => {
  useNotifier(props.buffer);

  return (
    <Layout>
      <Chat connected {...props} />
    </Layout>
  );
};

export default App;
