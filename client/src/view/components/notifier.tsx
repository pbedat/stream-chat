import { ChatMessage } from "../../types";
import { useEffect, useState, useRef } from "react";

export function useNotifier(buffer: ChatMessage[]) {
  const [muted, setMuted] = useState(true);

  const sound = useRef(
    new Audio(
      "https://notificationsounds.com/soundfiles/a86c450b76fb8c371afead6410d55534/file-sounds-1108-slow-spring-board.ogg"
    )
  );

  const mute = () => setMuted(true);
  const unmute = () => setMuted(false);

  useEffect(() => {
    if (
      Notification.permission !== "denied" &&
      Notification.permission !== "granted"
    ) {
      Notification.requestPermission(function(permission) {
        new Notification("Allright!");
      });
    }

    window.addEventListener("focus", mute);
    window.addEventListener("blur", unmute);

    return () => {
      window.removeEventListener("focus", mute);
      window.removeEventListener("blur", unmute);
    };
  }, []);

  const notificationRef = useRef<Notification>();

  useEffect(() => {
    if (muted) {
      return;
    }
    sound.current.play();
    const message = buffer[buffer.length - 1];
    if (notificationRef.current) {
      notificationRef.current.close();
    }
    const notification = new Notification(`${message.nickName}`, {
      body: message.text,
      vibrate: 1
    });
    notificationRef.current = notification;
  }, [buffer]);
}
