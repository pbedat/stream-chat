import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";

interface InputProps {
  onSubmit(msg: string): void;
}

const Input: React.FC<InputProps> = props => {
  const { onSubmit } = props;
  const [value, setValue] = useState("");

  const handleChange = (ev: React.ChangeEvent<HTMLInputElement>) =>
    setValue(ev.currentTarget.value);

  const handleSubmit = (ev: React.FormEvent) => {
    ev.preventDefault();
    if (value.trim().length) {
      onSubmit(value);
      setValue("");
    }
  };

  return (
    <form onSubmit={handleSubmit} style={{ display: "flex" }}>
      <input
        placeholder="Write a message..."
        value={value}
        onChange={handleChange}
        style={{ flexGrow: 1, fontSize: "1rem", padding: "0.75rem" }}
      />
      {value.trim().length > 0 && (
        <button>
          <FontAwesomeIcon icon={faPaperPlane} />
        </button>
      )}
    </form>
  );
};

export default Input;
