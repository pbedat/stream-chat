import React, { useRef, useEffect } from "react";
import { ChatMessage } from "../../types";
import { format, startOfDay } from "date-fns";

interface Props {
  buffer: ChatMessage[];
}

const MessageBuffer: React.FC<Props> = props => {
  const { buffer } = props;
  const lastRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    lastRef.current && lastRef.current.scrollIntoView();
  }, [props.buffer]);
  return (
    <>
      {buffer.map(({ id, text, nickName, timestamp, isOwn }, i) => (
        <div
          key={id}
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: isOwn ? "row-reverse" : "row"
          }}
          ref={i === buffer.length - 1 ? lastRef : undefined}
        >
          <Triangle
            direction={isOwn ? "right" : "left"}
            color={isOwn ? "#e5ffff" : "white"}
          />
          <div
            style={{
              background: isOwn ? "#e5ffff" : "white",
              textAlign: isOwn ? "right" : "left",
              padding: "0.5rem",
              marginBottom: 2
            }}
          >
            {!isOwn && (
              <small>
                <strong style={{ color: "gray" }}>{nickName}</strong>
              </small>
            )}
            <div>
              {text}{" "}
              <small style={{ marginLeft: "2rem" }}>
                {formatTimestamp(timestamp)}
              </small>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

function formatTimestamp(timestamp: Date) {
  if (startOfDay(timestamp).getTime() === startOfDay(new Date()).getTime()) {
    return format(timestamp, "HH:mm");
  }
  return format(timestamp, "dd.MM.yyyy HH:mm");
}

export default MessageBuffer;

interface TriangleProps {
  direction: "left" | "right";
  color: string;
}

const Triangle: React.FC<TriangleProps> = props => {
  const { direction, color } = props;
  const style = {
    width: 0,
    height: 0,
    borderTop: "8px solid transparent",
    borderBottom: "8px solid transparent",
    borderLeft: direction === "right" ? `10px solid  ${color}` : undefined,
    borderRight: direction === "left" ? `10px solid  ${color}` : undefined
  };
  return <div style={style} />;
};
