import React from "react";

/**
 * Applies a layout to the children of the component.
 * The Layout result in a padded and centered full height and full width flexbox.
 */
const Layout: React.FC = props => {
  const { children } = props;
  return (
    <div
      style={{
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center"
      }}
    >
      <div
        style={{
          display: "flex",
          paddingTop: "5%",
          paddingBottom: "5%",
          paddingLeft: "10%",
          paddingRight: "10%",
          flexGrow: 1,
          justifyContent: "center",
          backgroundColor: "white"
        }}
      >
        <div
          style={{
            background: "#E1E2E1",
            padding: "1rem",
            flexGrow: 1,
            display: "flex",
            flexDirection: "column"
          }}
        >
          {children}
        </div>
      </div>
    </div>
  );
};

export default Layout;
