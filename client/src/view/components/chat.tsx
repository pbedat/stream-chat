import React from "react";

import { ChatMessage } from "../../types";
import Input from "./input";
import MessageBuffer from "./buffer";

interface ChatProps {
  connected: boolean;
  buffer: ChatMessage[];
  onSubmit(msg: string): void;
}

const Chat: React.FC<ChatProps> = props => {
  const { onSubmit, buffer, connected } = props;
  return (
    <div style={{ flexGrow: 1, display: "flex", flexDirection: "column" }}>
      {!connected && <div>OFFLINE</div>}
      <div
        style={{
          flex: "1",
          flexBasis: 0,
          overflowY: "auto",
          paddingBottom: "1rem"
        }}
      >
        <MessageBuffer buffer={buffer} />
      </div>
      <Input onSubmit={onSubmit} />
    </div>
  );
};

export default Chat;
