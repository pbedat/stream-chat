# Develop

Start the server and client:

```
cd server && npm start
cd client && npm start
```

Browse to the ['develop' chat](http://127.0.0.1:3000?streamName=develop&nickName=developer)
